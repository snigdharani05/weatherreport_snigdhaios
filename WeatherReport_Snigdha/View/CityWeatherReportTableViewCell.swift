//
//  CityWeatherReportTableViewCell.swift
//  WeatherReport_Snigdha
//
//  Created by HTS on 01/05/21.
//

import UIKit

class CityWeatherReportTableViewCell: UITableViewCell {
    
    @IBOutlet var day_Label: UILabel!
    @IBOutlet var temp_Label: UILabel!
    @IBOutlet var feelsLike_Label: UILabel!
    
    @IBOutlet var tempHigh_Label: UILabel!
    @IBOutlet var tempLow_Label: UILabel!
    
    @IBOutlet var humidity: UILabel!
    @IBOutlet var pressure_LAbel: UILabel!
    
    @IBOutlet var precipitation_Label: UILabel!
    
    @IBOutlet var wind_Label: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
