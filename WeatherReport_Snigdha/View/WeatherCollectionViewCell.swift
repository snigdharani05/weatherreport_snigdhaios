//
//  WeatherCollectionViewCell.swift
//  WeatherReport_Snigdha
//
//  Created by HTS on 01/05/21.
//

import UIKit

class WeatherCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var title_Label: UILabel!
    
    @IBOutlet var value_Label: UILabel!
    
}
