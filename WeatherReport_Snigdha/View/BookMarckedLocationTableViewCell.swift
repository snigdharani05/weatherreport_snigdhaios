//
//  BookMarckedLocationTableViewCell.swift
//  WeatherReport_Snigdha
//
//  Created by HTS on 01/05/21.
//

import UIKit

class BookMarckedLocationTableViewCell: UITableViewCell {
    
    @IBOutlet var btn_RemoveLocation: UIButton!
    
    
    @IBOutlet var LocationName: UILabel!
    
    @IBOutlet var Address: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
