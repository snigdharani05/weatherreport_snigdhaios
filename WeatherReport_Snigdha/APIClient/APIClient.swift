//
//  APIClient.swift
//  WeatherReport_Snigdha
//
//  Created by HTS on 02/05/21.
//

import Foundation

var BaseURL = "http://api.openweathermap.org/data/2.5/"
var APIKey = "fae7190d7e6433ec3a45285ffcf55c86"
var appid = "&appid="
var oneDayWeatherReport = "weather?"
var fiveDayWeatherReport = "forecast?"
var lattitude = "lat="
var longitude = "&lon="
var units = "&units="

class APIClient  {
    

    
    static func APIGetServices(stringURL: String, withCompletionHandler:@escaping (_ response:Any) -> Void) {
        let session = URLSession.shared
        let url = URL(string: stringURL)!

        let task = session.dataTask(with: url, completionHandler: { data, response, error in
            // Check the response
                    print(error)
                    print(response)
            // Serialize the data into an object
                        do {
                            let json = try JSONSerialization.jsonObject(with: data!, options: [])
                            print(json)
                            withCompletionHandler(json)
                        } catch {
                            print("Error during JSON serialization: \(error.localizedDescription)")
                        }
            })
            task.resume()
    }
    
    
    
    
    
}
