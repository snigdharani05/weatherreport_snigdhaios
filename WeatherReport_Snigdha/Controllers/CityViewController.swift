//
//  CityViewController.swift
//  WeatherReport_Snigdha
//
//  Created by HTS on 01/05/21.
//

import UIKit

class CityViewController: UIViewController {
    
    @IBOutlet var city_Label: UILabel!
    @IBOutlet var temperature_Label: UILabel!
    @IBOutlet var collectionView: UICollectionView!
    
    @IBOutlet var weatherForcastListTableView: UITableView!
    
    var city_lattitude = Double()
    var city_longitude = Double()
    var cityName = String()
    
    
    var BaseURL = "http://api.openweathermap.org/data/2.5/"
    var APIKey = "fae7190d7e6433ec3a45285ffcf55c86"
    var appid = "&appid="
    var oneDayWeatherReport = "weather?"
    var fiveDayWeatherReport = "forecast?"
    var lattitude = "lat="
    var longitude = "&lon="
    var units = "&units="
    
    var Obj_CityWeatherReportModel = CityWeatherReportModel(dic: [String:AnyObject]())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        city_Label.text = cityName
        getOneDayForcast()
        
        
        weatherForcastListTableView.delegate = self
        weatherForcastListTableView.dataSource = self
        weatherForcastListTableView.tableFooterView = UIView()
        weatherForcastListTableView.backgroundColor = .clear
        
    }
    
    
    func getOneDayForcast(){
        let urlString = "\(BaseURL)\(oneDayWeatherReport)\(lattitude)\(city_lattitude)\(longitude)\(city_longitude)\(appid)\(APIKey)\(units)metric"
        
        APIClient.APIGetServices(stringURL: urlString){(response) in            
            if let resp = response as? [String: AnyObject]
            {
                print(resp)
                self.Obj_CityWeatherReportModel = CityWeatherReportModel.init(dic: resp)
            }
            
            
            DispatchQueue.main.async {
                self.temperature_Label.text = "\(self.Obj_CityWeatherReportModel.temp)"
                self.collectionView.reloadData()
            }
            
        }
        
    }
    
    func getFiveDayForcast(){
       //http://api.openweathermap.org/data/2.5/forecast?lat=0&lon=0&appid=fae7190d7e6433ec3a45285ffcf55c86&units=metric
    }
    
    
    

}

extension CityViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WeatherCollectionViewCell", for: indexPath) as? WeatherCollectionViewCell ?? WeatherCollectionViewCell()
        
        switch indexPath.row {
        case 0:
            cell.title_Label.text = "Weather"
            if self.Obj_CityWeatherReportModel.weathers.count > 0{
                cell.value_Label.text = "\(self.Obj_CityWeatherReportModel.weathers[0].description)"
            }
            break
        case 1:
            cell.title_Label.text = "Feels Like"
            cell.value_Label.text = "\(self.Obj_CityWeatherReportModel.feels_like)"
            break
        case 2:
            cell.title_Label.text = "H/L"
            cell.value_Label.text = "\(self.Obj_CityWeatherReportModel.temp_max)/\(self.Obj_CityWeatherReportModel.temp_min)"
            break
        case 3:
            cell.title_Label.text = "Humidity"
            cell.value_Label.text = "\(self.Obj_CityWeatherReportModel.humidity)"
            break
        case 4:
            cell.title_Label.text = "Wind"
            cell.value_Label.text = "\(self.Obj_CityWeatherReportModel.Wind_speed)"
            break
        case 5:
            cell.title_Label.text = "Pressure"
            cell.value_Label.text = "\(self.Obj_CityWeatherReportModel.pressure)"
            break
        default:
            break
        }
        
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.size.width / 3.2, height: self.collectionView.frame.size.height / 2.2)
    }
    
}

extension CityViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CityReportCell") as? CityWeatherReportTableViewCell ?? CityWeatherReportTableViewCell()
        
        return cell
    }
    
    
}


