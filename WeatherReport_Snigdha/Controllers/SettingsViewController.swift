//
//  SettingsViewController.swift
//  WeatherReport_Snigdha
//
//  Created by HTS on 01/05/21.
//

import UIKit

class SettingsViewController: UIViewController {
    @IBOutlet var unit_Label: UILabel!
    
    @IBOutlet var switch_Unit: UISwitch!
    
    @IBOutlet var deleteList_Label: UILabel!
    
    @IBOutlet var btn_DeleteList: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    
    @IBAction func deleteLocationList(_ sender: Any) {
        
        
        CoreDataModel.sharedInstance.deleteAllDataFromCoreData()
    }
    
    
    @IBAction func unitSwictAction(_ sender: Any) {
    }
    
    

}
