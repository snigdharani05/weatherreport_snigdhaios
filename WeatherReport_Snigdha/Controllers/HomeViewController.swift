//
//  HomeViewController.swift
//  WeatherReport_Snigdha
//
//  Created by HTS on 01/05/21.
//

import UIKit
import MapKit
import Foundation
import CoreLocation

class HomeViewController: UIViewController {
    
    @IBOutlet var searchBar: UISearchBar!
    
    @IBOutlet var BokkmarkedLocationsTableView: UITableView!
    
    @IBOutlet var btn_Menu: UIButton!
    
    
     
    private var originalPullUpControllerViewSize: CGSize = .zero
    
    var LocationArray = [BookMarkLocation]()
    var FilteredLocationArray = [BookMarkLocation]()
    
    
    var SearchBarValue:String!
    var searchActive : Bool = false
        
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.delegate = self
        BokkmarkedLocationsTableView.delegate = self
        BokkmarkedLocationsTableView.dataSource = self
        BokkmarkedLocationsTableView.tableFooterView = UIView()
        BokkmarkedLocationsTableView.backgroundColor = .clear
        
        CoreDataModel.sharedInstance.fetchingData(){(response ) in
            
            self.LocationArray = response
            DispatchQueue.main.async { [self] in
                BokkmarkedLocationsTableView.reloadData()
            }
            
        }
        
        

        let pullUpController = makeSearchViewControllerIfNeeded()
        removePullUpController(pullUpController, animated: true)

        if children.first(where: { String(describing: $0.classForCoder) == "PullUpViewController" }) == nil {
            addPullUpController(animated: true)
        }else{
            print("we have one")
        }
        
    }
    
    
    @IBAction func menuAction(_ sender: Any) {
        
        let actionSheet: UIAlertController = UIAlertController(title: "Please select", message: "Option to select", preferredStyle: .actionSheet)

            let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
                print("Cancel")
            }
        actionSheet.addAction(cancelActionButton)

            let settings = UIAlertAction(title: "settings", style: .default)
                { _ in
                   
                let settingVC = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as? SettingsViewController ?? SettingsViewController()

                self.navigationController?.pushViewController(settingVC, animated: true)
                
            }
        actionSheet.addAction(settings)

            let help = UIAlertAction(title: "help", style: .default)
                { _ in
                let helpVC = self.storyboard?.instantiateViewController(withIdentifier: "HelpViewController") as? HelpViewController ?? HelpViewController()

                self.navigationController?.pushViewController(helpVC, animated: true)
            }
        
        actionSheet.addAction(help)
        self.present(actionSheet, animated: true, completion: nil)
        
    }

}

extension HomeViewController: UISearchBarDelegate
{
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
            searchActive = true
        }

        func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
            searchActive = false
        }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
            searchActive = false;

            searchBar.text = nil
            searchBar.resignFirstResponder()
            BokkmarkedLocationsTableView.resignFirstResponder()
            self.searchBar.showsCancelButton = false
            BokkmarkedLocationsTableView.reloadData()
        }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
            searchActive = false
        }

    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
                    return true
        }


    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
                
        self.searchActive = true;
                
        self.searchBar.showsCancelButton = true
                
        FilteredLocationArray.removeAll()

        self.FilteredLocationArray =  self.LocationArray.filter{ ($0.address?.contains(searchText) ?? false)}
        self.BokkmarkedLocationsTableView.reloadData()

        }
}


extension HomeViewController : UITableViewDelegate, UITableViewDataSource{
    //MARK:- Tableview Delegates and Datasource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        

        return LocationArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let customCell  = tableView.dequeueReusableCell(withIdentifier: "BookMarkedLocationCell") as? BookMarckedLocationTableViewCell ?? BookMarckedLocationTableViewCell()
        customCell.LocationName.text = LocationArray[indexPath.row].locationName
        customCell.Address.text = LocationArray[indexPath.row].address
        customCell.Address.textColor = .black
        customCell.btn_RemoveLocation.tag = indexPath.row
        customCell.btn_RemoveLocation.isHidden = false
        customCell.btn_RemoveLocation.addTarget(self, action: #selector(self.RemoveLocation(sender:)), for: .touchUpInside)
        return customCell
    }
    
    @objc func RemoveLocation(sender:UIButton){

        CoreDataModel.sharedInstance.deleteSelectedObjectFromCoreData(index: sender.tag)
        self.LocationArray.remove(at: sender.tag)
//        self.bookMarkedLocationTableView.deleteRows(at: [indexPath], with: .fade)
        self.BokkmarkedLocationsTableView.reloadData()

        let pullUpController = makeSearchViewControllerIfNeeded()
        _ = pullUpController.view
        pullUpController.mapReload()
//        removePullUpController(pullUpController, animated: true)
//
//        addPullUpController(animated: true)
//        if children.first(where: { String(describing: $0.classForCoder) == "PullUpViewController" }) == nil {
//
//        }else{
//            print("we have one")
//        }
       
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
    
      if editingStyle == .delete {
        print("Deleted")

        CoreDataModel.sharedInstance.deleteSelectedObjectFromCoreData(index: indexPath.row)
        self.LocationArray.remove(at: indexPath.row)
        self.BokkmarkedLocationsTableView.deleteRows(at: [indexPath], with: .fade)
      }
    }
    
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 100//UITableView.automaticDimension
//    }
//
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 100
//    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cityView = self.storyboard?.instantiateViewController(withIdentifier: "CityViewController") as? CityViewController ?? CityViewController()
        cityView.cityName = LocationArray[indexPath.row].locationName ?? ""
        cityView.city_longitude = LocationArray[indexPath.row].longitude
        cityView.city_lattitude = LocationArray[indexPath.row].latitude

        self.navigationController?.pushViewController(cityView, animated: true)

    }
}




extension HomeViewController : PullUpViewControllerDelegate {
    
    func PullUpViewControllerDelegateMethod() {
        CoreDataModel.sharedInstance.fetchingData(){(response ) in
            
            self.LocationArray = response
            DispatchQueue.main.async { [self] in
                BokkmarkedLocationsTableView.reloadData()
            }
            
        }
    }
    
    
    private func makeSearchViewControllerIfNeeded() -> PullUpViewController {
        let currentPullUpController = children
            .filter({ $0 is PullUpViewController })
            .first as? PullUpViewController
        let pullUpController: PullUpViewController = currentPullUpController ?? UIStoryboard(name: "Main",bundle: nil).instantiateViewController(withIdentifier: "PullUpViewController") as? PullUpViewController ?? PullUpViewController()
        //           if initialStateSegmentedControl.selectedSegmentIndex == 0 {
        //               pullUpController.initialState = .contracted
        //           } else {
        //               pullUpController.initialState = .expanded
        //           }
        
        pullUpController.initialState = .contracted
//        pullUpController.arrayItems = array_MultiTripDetails
//        pullUpController.info_Dict = info_Dict
        pullUpController.delegate = self
//        pullUpController.pullUpSelectedIndex = pullUpSelectedIndex
        if originalPullUpControllerViewSize == .zero {
            originalPullUpControllerViewSize = pullUpController.view.bounds.size
        }
        return pullUpController
    }
    
    private func addPullUpController(animated: Bool) {
        let pullUpController = makeSearchViewControllerIfNeeded()
        _ = pullUpController.view // call pullUpController.viewDidLoad()
        addPullUpController(pullUpController,
                            initialStickyPointOffset: pullUpController.initialPointOffset,
                            animated: animated)
    }
    
}



