//
//  HelpViewController.swift
//  WeatherReport_Snigdha
//
//  Created by HTS on 01/05/21.
//

import UIKit
import WebKit

class HelpViewController: UIViewController {
    
    
    let subView = UIView()
    var activityView : UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        let myURLString = "https://openweathermap.org/guide"
        let url = URL(string: myURLString)
        let request = URLRequest(url: url!)
        
        let webView = WKWebView(frame: self.view.frame)
        webView.navigationDelegate = self
        webView.load(request)
        view.addSubview(webView)
        
        subView.frame.size = CGSize(width: 100, height: 100)
        subView.center = self.view.center
        self.view.addSubview(subView)
        subView.backgroundColor = UIColor.gray
        
        activityView = UIActivityIndicatorView(style: .large)
        activityView.center = CGPoint(x: subView.bounds.size.width/2, y: subView.bounds.size.height/2)
        activityView.startAnimating()
        activityView.color = UIColor.white
        subView.addSubview(activityView)

    }
    

}

extension HelpViewController: WKNavigationDelegate {

    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Started to load")
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("Finished loading")
        
        
        activityView.stopAnimating()
        subView.removeFromSuperview()
    }

    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
    }
}
