//
//  PullUpViewController.swift
//  WeatherReport_Snigdha
//
//  Created by HTS on 01/05/21.
//

import UIKit
import CoreData
import MapKit
import CoreLocation

extension UIViewController {

    var hasSafeArea: Bool {
        guard
            #available(iOS 11.0, tvOS 11.0, *)
            else {
                return false
            }
        return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 20
    }

}

@objc protocol PullUpViewControllerDelegate
{
    @objc optional func PullUpViewControllerDelegateMethod()
}

class PullUpViewController: PullUpController  {
    
    
    @IBOutlet var mapView: MKMapView!
    var locationManager = CLLocationManager()
    
    var tapped_cityString = ""
    var tapped_address = ""
    
    enum InitialState {
          case contracted
          case expanded
      }
      
      var initialState: InitialState = .contracted
      var initialPointOffset: CGFloat {
          switch initialState {
          case .contracted:
              return 200
          case .expanded:
              return pullUpControllerPreferredSize.height
          }
      }
    var delegate : PullUpViewControllerDelegate?
    
    public var portraitSize: CGSize = .zero
    public var landscapeFrame: CGRect = .zero

    private var safeAreaAdditionalOffset: CGFloat {
        hasSafeArea ? 20 : 0
    }
    
    
    var selectedIndexPath = 0
    var LocationArray = [BookMarkLocation]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        portraitSize = CGSize(width: min(UIScreen.main.bounds.width, UIScreen.main.bounds.height),
                              height: 180)
        landscapeFrame = CGRect(x: 5, y: 50, width: 280, height: 300)
        
        
        self.mapView.delegate = self
//        self.MapView.showsUserLocation = true
//        self.locationManager.delegate = self;
//        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        //self.locationManager.requestWhenInUseAuthorization()
//        self.mapView.showsUserLocation = true
//        self.locationManager.startUpdatingLocation()
        
        
        CoreDataModel.sharedInstance.fetchingData(){(response ) in
            
            self.LocationArray = response
            DispatchQueue.main.async {
                for loc in self.LocationArray{
                    let annotation = MKPointAnnotation()
                    annotation.coordinate = CLLocationCoordinate2D(latitude: loc.latitude, longitude: loc.longitude)
                    self.mapView.addAnnotation(annotation)
                }
            }
            
        }
        
        
        let lpgr = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress(gestureRecognizer:)))
          lpgr.minimumPressDuration = 0.5
          lpgr.delaysTouchesBegan = true
          lpgr.delegate = self
          self.mapView.addGestureRecognizer(lpgr)
        
    }
    
    
    func mapReload(){
        self.mapView.removeAnnotations(self.mapView.annotations)

        CoreDataModel.sharedInstance.fetchingData(){(response ) in
            
            self.LocationArray = response
            DispatchQueue.main.async {
                for loc in self.LocationArray{
                    let annotation = MKPointAnnotation()
                    annotation.coordinate = CLLocationCoordinate2D(latitude: loc.latitude, longitude: loc.longitude)
                    self.mapView.addAnnotation(annotation)
                }
            }
            
        }
    }
    
    
    @objc func handleLongPress(gestureRecognizer: UILongPressGestureRecognizer) {

            if gestureRecognizer.state == UIGestureRecognizer.State.ended {
                //return
                let touchPoint = gestureRecognizer.location(in: self.mapView)
                let yourAnnotation = MKPointAnnotation()
                let touchMapCoordinate =  self.mapView.convert(touchPoint, toCoordinateFrom: mapView)
                yourAnnotation.subtitle = "You long pressed here"
                yourAnnotation.coordinate = touchMapCoordinate
                self.mapView.addAnnotation(yourAnnotation)
                print("handled tap")
                getAddressFromLatLon(pdblLatitude: touchMapCoordinate.latitude, pdblLongitude: touchMapCoordinate.longitude){(success) in
                    
                    if success{
                        CoreDataModel.sharedInstance.saveData(LocationName: self.tapped_cityString, LocationAddress: self.tapped_address, Latitude: touchMapCoordinate.latitude, Longitude: touchMapCoordinate.longitude)
                        self.delegate?.PullUpViewControllerDelegateMethod?()
                    }else{
                        //CoreDataModel.sharedInstance.saveData(LocationName: "NA", LocationAddress: "NA", Latitude: touchMapCoordinate.latitude, Longitude: touchMapCoordinate.longitude)
                    }
                    
                }
                
            }
            
    }
    
    func getAddressFromLatLon(pdblLatitude: Double, pdblLongitude: Double, withCompletionHandler:@escaping (_ success:Bool) -> Void) {
            var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
            let lat: Double = Double(pdblLatitude)
            //21.228124
            let lon: Double = Double(pdblLongitude)
            //72.833770
        
        print(lat, lon)
            let ceo: CLGeocoder = CLGeocoder()
            center.latitude = lat
            center.longitude = lon

            let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)

        

            ceo.reverseGeocodeLocation(loc, completionHandler:
                {(placemarks, error) in
                    if (error != nil)
                    {
                        withCompletionHandler(false)
                        print("reverse geodcode fail: \(error!.localizedDescription)")
                        return
                    }
                    if placemarks == nil{
                        withCompletionHandler(false)
                        return
                    }
                    let pm = placemarks! as [CLPlacemark]

                    if pm.count > 0 {
                        let pm = placemarks![0]
                        print(pm.country)
                        print(pm.locality)
                        print(pm.subLocality)
                        print(pm.thoroughfare)
                        print(pm.postalCode)
                        print(pm.subThoroughfare)
                        var addressString : String = ""
                        if pm.subLocality != nil {
                            addressString = addressString + pm.subLocality! + ", "
                        }
                        if pm.thoroughfare != nil {
                            addressString = addressString + pm.thoroughfare! + ", "
                        }
                        if pm.locality != nil {
                            addressString = addressString + pm.locality! + ", "
                        }
                        if pm.country != nil {
                            addressString = addressString + pm.country! + ", "
                        }
                        if pm.postalCode != nil {
                            addressString = addressString + pm.postalCode! + " "
                        }


                        self.tapped_cityString = pm.locality ?? ""
                        self.tapped_address = addressString
                        print(addressString)
                        withCompletionHandler(true)
                        
                  }
            })

        }

    
    
    
//    override func viewWillAppear() {
//        super.viewWillAppear()
//
//
//    }
    
    
    // MARK: - PullUpController
       
   override var pullUpControllerPreferredSize: CGSize {
       return CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - 100 )
   }

   override func pullUpControllerWillMove(to stickyPoint: CGFloat) {
       //        print("will move to \(stickyPoint)")
   }
   
   override func pullUpControllerDidMove(to stickyPoint: CGFloat) {
       //        print("did move to \(stickyPoint)")
   }
   
   override func pullUpControllerDidDrag(to point: CGFloat) {
               print("did drag to \(point)")
//       if point == 0.0{
//        self.willMove(toParent: nil)
//           self.view.removeFromSuperview()
//        self.removeFromParent()
//       }
   }
   
   // MARK: - PullUpController
      

      
      override var pullUpControllerPreferredLandscapeFrame: CGRect {
          return landscapeFrame
      }
      
      override var pullUpControllerMiddleStickyPoints: [CGFloat] {
          switch initialState {
          case .contracted:
           return [20]
          case .expanded:
              return [180, 20]
          }
      }
      
      override var pullUpControllerBounceOffset: CGFloat {
          return 20
      }
      
      override func pullUpControllerAnimate(action: PullUpController.Action,
                                            withDuration duration: TimeInterval,
                                            animations: @escaping () -> Void,
                                            completion: ((Bool) -> Void)?) {
          switch action {
          case .move:
              UIView.animate(withDuration: 0.3,
                             delay: 0,
                             usingSpringWithDamping: 0.7,
                             initialSpringVelocity: 0,
                             options: .curveEaseInOut,
                             animations: animations,
                             completion: completion)
          default:
              UIView.animate(withDuration: 0.3,
                             animations: animations,
                             completion: completion)
          }
      }
    

   
}

extension PullUpViewController: MKMapViewDelegate{
    func mapViewWillStartLoadingMap(_ mapView: MKMapView) {
        
    }
    
    
    
    func mapViewDidFinishLoadingMap(_ mapView: MKMapView) {
        
    }
    
}

extension PullUpViewController: CLLocationManagerDelegate{
    
}


extension PullUpViewController: UIGestureRecognizerDelegate{
    
}



