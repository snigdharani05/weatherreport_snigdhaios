//
//  BookMarkLocation+CoreDataProperties.swift
//  WeatherReport_Snigdha
//
//  Created by HTS on 01/05/21.
//
//

import Foundation
import CoreData


extension BookMarkLocation {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<BookMarkLocation> {
        return NSFetchRequest<BookMarkLocation>(entityName: "BookMarkLocation")
    }

    @NSManaged public var locationName: String?
    @NSManaged public var latitude: Double
    @NSManaged public var longitude: Double
    @NSManaged public var address: String?

}

extension BookMarkLocation : Identifiable {

}
