//
//  CityWeatherReportModel.swift
//  WeatherReport_Snigdha
//
//  Created by HTS on 01/05/21.
//

import Foundation

class CityWeatherReportModel {
    
    var longitude = Double()
    var lattitude = Double()
    var weathers = [WeatherModel]()
    
    var temp = Double()
    var feels_like = Double()
    var temp_min = Double()
    var temp_max = Double()
    var pressure = Int()
    var humidity = Int()
//    var sea_level": 1010,
//    var grnd_level": 1010
    
    var visibility = Int()
    
    var Wind_speed = Double()
    
    
    init(dic: [String: AnyObject]) {
        if let coord = dic["coord"] as? [String:AnyObject]{
            longitude = coord["lon"] as? Double ?? 0.0
            lattitude = coord["lat"] as? Double ?? 0.0
        }
        
        if let arr_Weather = dic["weather"] as? [[String:AnyObject]]
        {
            self.weathers = arr_Weather.map { WeatherModel.init(dic: $0) }
        }
        
        if let main = dic["main"] as? [String:AnyObject]{
            temp = main["temp"] as? Double ?? 0.0
            feels_like = main["feels_like"] as? Double ?? 0.0
            temp_min = main["temp_min"] as? Double ?? 0.0
            temp_max = main["temp_max"] as? Double ?? 0.0
            pressure = main["pressure"] as? Int ?? 0
            humidity = main["humidity"] as? Int ?? 0
        }
        
        visibility = dic["visibility"] as? Int ?? 0
        
        if let wind = dic["wind"] as? [String:AnyObject]{
            Wind_speed = wind["speed"] as? Double ?? 0.0
        }
        
        
        
        
    }
    
    
    
    
    
}



class WeatherModel{
    
    var id = Int()
    var main = String()
    var description = String()
    
    
    init(dic: [String: AnyObject]) {
        
        id = dic["id"] as? Int ?? 0
        main = dic["main"] as? String ?? ""
        description = dic["description"] as? String ?? ""
    }
    
}
