//
//  CoreDataModel.swift
//  WeatherReport_Snigdha
//
//  Created by HTS on 01/05/21.
//

import Foundation
import UIKit
import CoreData

class CoreDataModel {
    
    static let sharedInstance: CoreDataModel = {
        let instace = CoreDataModel()
        return instace
    }()
    
    let mainContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext    
    
    func saveData(LocationName: String, LocationAddress: String, Latitude:Double, Longitude: Double)  {
       
        let entity = NSEntityDescription.entity(forEntityName: "BookMarkLocation", in: mainContext)
        let newLocation = NSManagedObject(entity: entity!, insertInto: mainContext)
        newLocation.setValue(LocationName, forKey: "locationName")
        newLocation.setValue(LocationAddress, forKey: "address")
        newLocation.setValue(Latitude, forKey: "latitude")
        newLocation.setValue(Longitude, forKey: "longitude")
        
        do {
            try mainContext.save()
        } catch let error {
            print("save error is \(error.localizedDescription)")
        }
    }
    
    func fetchingData(withCompletionHandler:@escaping (_ response:[BookMarkLocation]) -> Void) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "BookMarkLocation")
        var data = [BookMarkLocation]()
        do  {
            let BookMarkLocations = try mainContext.fetch(fetchRequest)
            for  loc in BookMarkLocations as! [BookMarkLocation] {
                data.append(loc)
            }

        } catch let errror {
            print("fetching error is \(errror.localizedDescription)")
        }
        withCompletionHandler(data)
    }
    
    
    func deleteAllDataFromCoreData() {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "BookMarkLocation")
        fetchRequest.returnsObjectsAsFaults = false
        do {
            let results =  try mainContext.fetch(fetchRequest)
            for object in results {
                guard let objectData = object as? NSManagedObject else {continue}
                mainContext.delete(objectData)
            }
        } catch let error {
            print("Detele all data  error :", error)
        }
    }
    
    
    func deleteSelectedObjectFromCoreData(index : Int){
        
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "BookMarkLocation")
        fetchRequest.returnsObjectsAsFaults = false
        do {
            let results =  try mainContext.fetch(fetchRequest)
            
            guard let objectData = results[index] as? NSManagedObject else {return}
            
            mainContext.delete(objectData)
            try mainContext.save()
        } catch let error {
            print("Detele data  error :", error)
        }
        
        
    }
        
    
}

